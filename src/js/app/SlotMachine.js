import React, { Component } from 'react';
import '../../assets/stylus/app.styl';

class Whell extends Component {

  generateNumber(){

    const x = Math.floor(Math.random() * 3);
    const y = Math.floor(Math.random() * 3);
    const z = Math.floor(Math.random() * 3);

    document.getElementById('demo').innerHTML = x;
  if (x === 0 ) {
    document.getElementById('x').style.backgroundColor = '#DC143C';
  } else if( x === 1) {
    document.getElementById('x').style.backgroundColor = '#00FFFF';
  } else if ( x === 2) {
    document.getElementById('x').style.backgroundColor = '#7FFF00';
  } else {
    document.getElementById('x').style.backgroundColor = '#00CED1';
  }


    document.getElementById('demo').innerHTML = y;
  if (y === 0 ) {
    document.getElementById('y').style.backgroundColor = '#DC143C';
  } else if ( y === 1) {
    document.getElementById('y').style.backgroundColor = '#00FFFF';
  } else if ( y === 2){
    document.getElementById('y').style.backgroundColor = '#7FFF00';
  } else {
    document.getElementById('y').style.backgroundColor = '#00CED1';
  }


    document.getElementById('demo').innerHTML = z;
  if (z === 0 ) {
    document.getElementById('z').style.backgroundColor = '#DC143C';
  } else if ( z === 1){
    document.getElementById('z').style.backgroundColor = '#00FFFF';
  } else if  ( z === 2){
    document.getElementById('z').style.backgroundColor = '#7FFF00';
  } else {
    document.getElementById('z').style.backgroundColor = '#00CED1';
  }

  if ((x === y) && (y === z) && (x === z)) {
    const q = document.getElementById('win').innerHTML = 'WINNER';
    if (q === 'WINNER'){
      document.getElementById('btn').disabled = 'true';
    }
  }
}

render() {
  return (
    <div className='hero'>
     <hr />
     <h1>Click PLAY to start the game </h1>
     <hr />
    <div className='container'>
       <div id="demo" className='dem'></div>
       <div id='x' className='circle' ></div>
       <div id="y" className='circle'></div>
       <div id="z" className='circle'></div>
    </div>
      <hr />
    <div id="win"></div>
      <hr />
      <button id='btn'type='button' onClick={ this.generateNumber }> PLAY </button>
    </div>
    )
  }
}

export default Whell
