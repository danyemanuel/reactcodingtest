import React, { Component } from 'react';
import '../../assets/stylus/app.styl';

class Footer extends Component {

  render() {
    return (
      <div className='footer'>
        powered by  <a href='http://www.freeslots.com/' className='link'> <strong>Slots Machine</strong></a>
      </div>

    )
  }
}

export default Footer
